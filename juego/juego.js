var assert = require('assert');
var Jugador = require('./jugador.js');
var Tablero = require('./tablero.js');

module.exports = function (instanciaAdministradorCartasPato
                           , instanciaAdministradorCartasEspeciales) {

    let self = this;

    var i = 0;

    this.jugadores = [];
    this.estado = 'esperando_jugadores';
    this.tablero = new Tablero();
    this.cartasEspeciales = [];
    this.administradorCartasPato = instanciaAdministradorCartasPato;
    this.administradorCartasEspeciales = instanciaAdministradorCartasEspeciales;

    this.agregarJugador = function(nombre, color_pato) {
        let jugador = new Jugador(nombre, color_pato);
        self.jugadores.push(jugador);
        return jugador;
    };

    this.sacarJugador = function(nombre) {
        let resultado = this.jugadores.filter(jugador=> jugador.nombre != nombre);

        console.log(resultado);

        self.jugadores = resultado;
    };

    this.devolverCartaEspecial = function(cartaEspecial) {
        self.administradorCartasEspeciales.devolverCartaEspecial(cartaEspecial);
    };

    this.agregarUnaCartaPatoAlTablero = function() {
        self.tablero.agregarCarta(self.administradorCartasPato.unaCarta());
    };

    this.llenarTableroDePatos = function() {
        // rellenamos los slots
        for (i=0; i< 6; i++)
            self.agregarUnaCartaPatoAlTablero();
    };
    
    this.iniciar = function(jugadorInicial) {
        self.estado = 'iniciado';
        self.jugadorInicial = jugadorInicial;

        self.indiceSiguienteJugador = self.jugadores.findIndex(x => x.nombre == self.jugadorInicial);

        // sacamos los colores activos en el mazo
        var coloresActivos = [];
        self.jugadores.forEach(x => coloresActivos.push(x.colorPato));
        self.administradorCartasPato.dejarSoloColoresActivos(coloresActivos);

        // repartimos las cartas
        self.jugadores.forEach(function(jugador) {
            jugador.agarrarCartaEspecial(self.administradorCartasEspeciales.unaCarta());
            jugador.agarrarCartaEspecial(self.administradorCartasEspeciales.unaCarta());
            jugador.agarrarCartaEspecial(self.administradorCartasEspeciales.unaCarta());
        });

        self.llenarTableroDePatos();
    };

    this.jugadorPorNombre = function(nombre) {
        var indice = self.jugadores.findIndex(x => x.nombre == nombre);
        return self.jugadores[indice];
    };

    this.jugarTurno = function(nombreJugador, numeroCarta, opciones) {
        let indice = parseInt(numeroCarta) - 1;
        let jugador = self.jugadorPorNombre(nombreJugador);

        var carta = jugador.iesimaCarta(indice);

        var descr = carta.ejecutar(self, self.tablero, self.jugadores, opciones);

        // el jugador toma una carta del mazo
        jugador.cartasEspeciales.splice(indice, 1);
        self.administradorCartasEspeciales.agregarCartaUsada(carta);
        jugador.cartasEspeciales.push(self.administradorCartasEspeciales.unaCarta());

        self.indiceSiguienteJugador += 1;

        if (self.indiceSiguienteJugador >= self.jugadores.length) {
            self.indiceSiguienteJugador = 0;
        }

        // se le pide al tablero que devuelva las cartas especiales que fueron usadas
        // y que duraban un turno
        var cartasEspecialesUsadasYLiberadas = self.tablero.terminoUnTurno();

        self.administradorCartasEspeciales.agregarAlMazoCartasQueEstabanEnElTablero(cartasEspecialesUsadasYLiberadas);

        return descr;
    };

    this.devolverElementosSacadosDelTablero = function(elementos) {
        for (var i = 0; i < elementos.length; i ++)
            self.administradorCartasPato.devolverCartaPato(elementos[i]);
    };

    this.proximoJugador = function() {
        return self.jugadores[self.indiceSiguienteJugador].nombre;
    };

    this.hayGanador = function() {
        var contadorPatosMuertos = self.jugadores.filter(x => x.patos.length == 5);
        return contadorPatosMuertos == self.jugadores.length-1;
    };

    this.jugadorGanador = function() {
        var jugadorGanador = self.jugadores.filter(x => x.patos.length < 5);
        return jugadorGanador.name;
    };

    this.estadoJuego = function() {
        return self.estado;
    };

    this.serializar = function() {
        return {
            "tablero": self.tablero.serializar()
            , "jugadores": self.jugadores
        }
    };
};