var Ordenador = require('./ordenador.js');

var CartaEspecialApuntar = require('./cartas_especiales/carta_especial_apuntar.js');
var CartaEspecialAvanzarPatos = require('./cartas_especiales/carta_especial_avanzar_patos.js');
var CartaEspecialDarleAlDeAlLado = require('./cartas_especiales/carta_especial_darle_al_de_al_lado.js');
var CartaEspecialDisparar = require('./cartas_especiales/carta_especial_disparar.js');
var CartaEspecialDobleDisparo = require('./cartas_especiales/carta_especial_doble_disparo.js');
var CartaEspecialDobleDisparoSoloSiHayPato = require('./cartas_especiales/carta_especial_doble_disparo_solo_si_hay_pato.js');
var CartaEspecialEsconderComoAvestruz = require('./cartas_especiales/carta_especial_esconder_como_avestruz.js');
var CartaEspecialIntercambiarPatoDeAdelante = require('./cartas_especiales/carta_especial_intercambiar_pato_de_adelante.js');
var CartaEspecialIntercambiarPatoDeAtras = require('./cartas_especiales/carta_especial_intercambiar_pato_de_atras.js');
var CartaEspecialMezclarPatosEnFilaConPatosEnMazo = require('./cartas_especiales/carta_especial_mezclar_patos_en_fila_con_patos_en_mazo.js');
var CartaEspecialMoverHastaAdelante = require('./cartas_especiales/carta_especial_mover_hasta_adelante_de_todo.js');
var CartaEspecialMoverMiraDerecha = require('./cartas_especiales/carta_especial_mover_mira_derecha.js');
var CartaEspecialMiraIzquierda = require('./cartas_especiales/carta_especial_mover_mira_izquierda.js');
var CartaEspecialOcultarDetrasDePatoAdyacente = require('./cartas_especiales/carta_especial_ocultar_detras_de_pato_adyacente.js');
var CartaEspecialQuickShot = require('./cartas_especiales/carta_especial_quick_shot.js');
var CartaEspecialReordenarAVoluntadPatosEnFila = require('./cartas_especiales/carta_especial_reordenar_a_voluntar_patos_en_fila.js');
var CartaEspecialResucitar = require('./cartas_especiales/carta_especial_resucitar.js');

module.exports = function () {

    let self = this;

    this.ordenador = new Ordenador();

    self.cartasEspeciales = [];

    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialApuntar());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialAvanzarPatos());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialDarleAlDeAlLado());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialDisparar());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialDobleDisparo());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialDobleDisparoSoloSiHayPato());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialEsconderComoAvestruz());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialIntercambiarPatoDeAdelante());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialIntercambiarPatoDeAtras());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialMezclarPatosEnFilaConPatosEnMazo());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialMoverHastaAdelante());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialMoverMiraDerecha());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialMiraIzquierda());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialOcultarDetrasDePatoAdyacente());
    for (i = 0; i < 5; i ++) this.cartasEspeciales.push(new CartaEspecialReordenarAVoluntadPatosEnFila());

    this.cartasEspeciales.push(new CartaEspecialQuickShot());
    this.cartasEspeciales.push(new CartaEspecialResucitar());

    this.cartasEspeciales = this.ordenador.reordenarObjetos(self.cartasEspeciales);

    this.cartasEspecialesUsadas = [];

    self.unaCarta = function() {
        var carta = self.cartasEspeciales.pop();
        if (self.cartasEspeciales.length === 0) {
            self.reciclarCartasUsadas();
        }
        return carta;
    };

    self.agregarCartaUsada = function(carta) {
        self.cartasEspecialesUsadas.push(carta);
    };

    self.reciclarCartasUsadas = function() {
        self.cartasEspeciales = self.ordenador.reordenarObjetos(self.cartasEspecialesUsadas);
        self.cartasEspecialesUsadas.length = 0;
    };

    self.devolverCartaEspecial = function(cartaEspecial) {
        self.cartasEspecialesUsadas.push(cartaEspecial);
    };

    self.agregarAlMazoCartasQueEstabanEnElTablero = function(cartas) {
        while (cartas.length > 0)
            self.devolverCartaEspecial(cartas.pop());
    };
};
