module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Apuntar!';
    this.codigo = 1;
    this.tipoEspecial = true;


    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var pos = parseInt(opciones)-1;
        tablero.ponerMira(pos);
        return 'se coloco una mira en la posicion ' + pos;
    };

};