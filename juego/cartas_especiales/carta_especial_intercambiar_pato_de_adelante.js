module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Intercambiar con pato de adelante';
    this.codigo = 8;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var partes = opciones.split(",");

        tablero.intercambiar(parseInt(partes[0]) - 1, parseInt(partes[1]) - 1);
        return 'se intercambiaron 2 patos';
    };
};