var ComponenteDisparar = require('./componente_disparar.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Errar y darle al de al lado';
    this.codigo = 3;
    this.tipoEspecial = true;

    this.componenteDisparar = new ComponenteDisparar();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(opciones) - 1);
        return self.descripcion;
    };
};