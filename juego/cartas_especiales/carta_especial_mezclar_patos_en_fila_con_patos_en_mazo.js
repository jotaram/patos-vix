var ComponenteAvanzarPatos = require('./componente_avanzar_patos.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Mezclar patos en fila con los patos del mazo y dar de nuevo';
    this.codigo = 10;
    this.tipoEspecial = true;
    this.componenteAvanzarPatos = new ComponenteAvanzarPatos();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {

        self.componenteAvanzarPatos.ejecutar(juego, tablero, 6);

        return 'se volvieron a poner todos los patos en el mazo y se repartio de nuevo';
    };
};