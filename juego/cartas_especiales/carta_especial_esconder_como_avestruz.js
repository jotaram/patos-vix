module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Esconder como avestruz';
    this.codigo = 7;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        tablero.ocultarPato(juego, parseInt(opciones) - 1, self);
        return self.descripcion;
    };
};