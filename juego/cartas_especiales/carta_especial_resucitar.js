module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Recusitar';
    this.codigo = 17;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        let jugador = juego.jugadorPorNombre(opciones);
        let msg = 'el jugador no resucito un pato';
        if (jugador.patos.length > 0) {
            let pato = jugador.patos.pop();
            juego.devolverElementosSacadosDelTablero([pato]);
            msg = 'el jugador ' + jugador.nombre + ' revivio un pato';
        }
        return msg;
    };
};