module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Disparar doble, pero solo si hay patos juntos';
    this.codigo = 6;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var partes = opciones.split(",");

        let pos1 = parseInt(partes[0]);
        let pos2 = parseInt(partes[1]);

        if (tablero.lago[pos1].cartasPato.length > 0) {
            if (tablero.lago[pos2].cartasPato.length > 0) {
                self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(partes[0]) - 1);
                self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(partes[1]) - 1);
            }
        }

        return self.descripcion;
    };
};