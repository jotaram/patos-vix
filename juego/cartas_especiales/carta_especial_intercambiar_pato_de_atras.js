module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Intercambiar con pato de atras';
    this.codigo = 9;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var partes = opciones.split(",");

        tablero.intercambiar(parseInt(partes[0]) - 1, parseInt(partes[1]) - 1);
        return 'se intercambio con el pato de atras';
    };
};