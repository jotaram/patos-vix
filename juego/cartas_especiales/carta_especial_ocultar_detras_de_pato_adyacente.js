module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Ocultar atras de pato adyacente';
    this.codigo = 14;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var partes = opciones.split(",");

        tablero.ocultarPatoDebajoDePato(parseInt(partes[0]) - 1, parseInt(partes[1]) - 1);
        return 'se oculto el pato';
    };
};