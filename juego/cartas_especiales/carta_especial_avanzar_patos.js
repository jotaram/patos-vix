var ComponenteAvanzarPatos = require('./componente_avanzar_patos.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Avanzar los patos';
    this.codigo = 2;
    this.tipoEspecial = true;

    this.componenteAvanzarPatos = new ComponenteAvanzarPatos();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        self.componenteAvanzarPatos.ejecutar(juego, tablero, 1);
        return 'se avanzaron los patos 1 casillero';
    };
};