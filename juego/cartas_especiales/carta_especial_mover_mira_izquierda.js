module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Mover mira a la izquierda';
    this.codigo = 13;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        let pos1 = parseInt(opciones) - 1;
        let pos2 = parseInt(opciones) - 2;

        if ((pos1 >= 0) && (pos1 < tablero.lago.length))
            if ((pos2 >= 0) && (pos2 < tablero.lago.length))
                tablero.moverMira(pos1, pos2);
        return 'se movio la mira a la izquierda';
    };
};