module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Reordenar los patos a voluntad';
    this.codigo = 16;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var posiciones = opciones.split(',');
        for (var i = 0; i < posiciones.length; i ++)
            posiciones[i] = parseInt(posiciones[i]) -1;

        tablero.reordenarObjetos(posiciones);
        return 'se reordeno el tablero como quiso el jugador';
    };
};