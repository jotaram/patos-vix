var ComponenteDisparar = require('./componente_disparar.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Quick Shot!';
    this.codigo = 15;
    this.tipoEspecial = true;

    this.componenteDisparar = new ComponenteDisparar();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        self.componenteDisparar.ejecutar(juego, tablero, jugadores, opciones);
        return 'se disparo de una modo quickshot';
    };
};