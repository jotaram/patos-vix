var ComponenteDisparar = require('./componente_disparar.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Disparar!';
    this.codigo = 4;
    this.tipoEspecial = true;

    this.componenteDisparar = new ComponenteDisparar();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(opciones) - 1);
        return self.descripcion;
    };
};