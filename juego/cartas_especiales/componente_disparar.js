module.exports = function () {

    this.ejecutar = function(juego, tablero, jugadores, indiceTarget) {
        var cartasRecuperadas = tablero.disparar(indiceTarget);

        var cartasParaDevolver = [];

        for (var i = 0; i < cartasRecuperadas.length; i ++) {
            var c = cartasRecuperadas[i];
            if (c.tipoEspecial) {
                juego.devolverCartaEspecial(c);
            }
            else {
                cartasParaDevolver.push(c);
            }
        }
        juego.devolverElementosSacadosDelTablero(cartasParaDevolver);

        var cantidadCartasPatoParaRellenear = 6 - tablero.lago.length;

        while (cantidadCartasPatoParaRellenear > 0) {
            juego.agregarUnaCartaPatoAlTablero();
            cantidadCartasPatoParaRellenear --;
        }
    };
};