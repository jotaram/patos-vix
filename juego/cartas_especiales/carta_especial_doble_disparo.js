var ComponenteDisparar = require('./componente_disparar.js');

module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Disparar Doble';
    this.codigo = 5;
    this.tipoEspecial = true;

    this.componenteDisparar = new ComponenteDisparar();

    this.ejecutar = function(juego, tablero, jugadores, opciones) {
        var partes = opciones.split(",");

        self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(partes[0]) - 1);
        self.componenteDisparar.ejecutar(juego, tablero, jugadores, parseInt(partes[1]) - 1);

        return self.descripcion;
    };
};