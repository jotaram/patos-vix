module.exports = function (descripcion) {

    let self = this;

    this.descripcion = 'Mover pato hasta adelante de todo';
    this.codigo = 11;
    this.tipoEspecial = true;

    this.ejecutar = function(juego, tablero, jugadores, opciones) {

        tablero.intercambiar(parseInt(opciones) - 1, 0);
        return 'se llevo el pato hasta adelante de todo';
    };
};