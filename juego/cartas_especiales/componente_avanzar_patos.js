module.exports = function () {

    this.ejecutar = function(juego, tablero, cantidad_avances) {

        var slotsRecuperados = [];

        while (cantidad_avances > 0) {
            slotsRecuperados.push(tablero.moverTodosHaciaAdelante());
            cantidad_avances --;
        }

        var cartasParaDevolver = [];

        for (var i = 0; i < slotsRecuperados.length; i ++) {
            var cartasPatoEnSlot = slotsRecuperados[i];
            for (var j = 0; j < cartasPatoEnSlot.length; j ++) {
                var c = cartasPatoEnSlot[i];
                if (c.tipoEspecial) {
                    juego.devolverCartaEspecial(c);
                }
                else {
                    cartasParaDevolver.push(c);
                }
            }
        }
        juego.devolverElementosSacadosDelTablero(cartasParaDevolver);

        while (tablero.lago.length < 6)
            juego.agregarUnaCartaPatoAlTablero();
    };
};