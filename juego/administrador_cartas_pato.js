var assert = require('assert');
var CartaAgua = require('./cartas_pato/carta_agua.js');
var CartaPato = require('./cartas_pato/carta_pato.js');
var CartasPorColor = require('./cartas_por_color');
var Ordenador = require('./ordenador.js');

module.exports = function () {

    let self = this;

    this.ordenador = new Ordenador();
    this.cartasPato = [];
    this.coloresDisponiblesParaPatos = [
        'azul'
        , 'amarillo'
        , 'naranja'
        , 'verde'
        , 'violeta'
        , 'rosa'
    ];

    this.rellenado = false;

    this.cartasAguaBase = [];

    for (var k = 0; k < 5; k ++)
        this.cartasAguaBase.push(new CartaAgua());

    this.mazoPatos = {};

    for (var i = 0; i < this.coloresDisponiblesParaPatos.length; i ++)
        this.mazoPatos[this.coloresDisponiblesParaPatos[i]] = new CartasPorColor(this.coloresDisponiblesParaPatos[i]);

    self.devolverCartaPato = function(cartaPato) {
        assert.equal(Array.isArray(cartaPato), false, 'me devuelven una carta pato invalida');
        self.cartasPato.push(cartaPato);
    };

    self.unaCarta = function() {
        assert(self.rellenado == true);
        assert(self.cartasPato.length >= 6);
        var c = self.cartasPato.pop();
        assert.equal(c === undefined, false);
        assert.equal(Array.isArray(c), false, 'la carta es un array');
        return c
    };

    self.dejarSoloColoresActivos = function(coloresActivos) {
        // assert(self.cartasPato.length === 0);

        while (self.cartasAguaBase.length > 0)
            self.cartasPato.push(self.cartasAguaBase.pop());

        coloresActivos.forEach(function(color) {
            var mazo = self.mazoPatos[color];
            for (var j = 0; j < mazo.cartasPato.length; j ++)
                self.cartasPato.push(mazo.cartasPato[j]);
        });
        self.cartasPato = self.ordenador.reordenarObjetos(self.cartasPato);
        self.rellenado = true;
    };

};
