var CartaPato = require('./cartas_pato/carta_pato.js');

module.exports = function (color) {

    let self = this;

    this.color= color;

    this.cartasPato = [];

    for (var i=0; i<5; i++)
        this.cartasPato.push(new CartaPato(this.color));
};
