module.exports = function (nombre, colorPato) {

    let self = this;

    this.nombre = nombre;
    this.colorPato = colorPato;
    this.patos = [];
    this.cartasEspeciales = [];

    this.agregarPato = function(pato) {
        this.patos.push(pato);
    };

    this.usarCartaEspecial = function (idCarta) {

    };

    this.agarrarCartaEspecial = function(carta) {
        this.cartasEspeciales.push(carta);
    };

    this.serializar = function() {
        var cartasEspecialesSerializadas = [];
        self.cartasEspeciales.forEach(function(cartaEspecial) {
            cartasEspecialesSerializadas.push(cartaEspecial.descripcion);
        });

        return {
            "nombre": self.nombre
            , "color": self.colorPato
            , "patos_muertos": self.patos.length
            , "cartas_especiales": cartasEspecialesSerializadas
        }
    };

    this.iesimaCarta = function(i) {
        // console.log('buscando la ieseima carta entre:');
        // console.log(self.cartasEspeciales);
        return self.cartasEspeciales[i];
    };
};
