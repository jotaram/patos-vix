var SlotCartaPato = require('./cartas_pato/slot_carta_pato.js');
var assert = require('assert');

module.exports = function () {

    let self = this;

    this.tamanio = 6;

    this.cielos = [0, 0, 0, 0, 0, 0];
    this.lago = [];
    this.contadorTurnos = 0;

    this.limpiarPoderesActivosPorUnTurno = function() {
        var cartasParaDevolver = [];

        for (var i = 0; i < self.lago.length; i ++) {
            var slot = self.lago[i];
            var j = 0;
            while (j < slot.cartasPato.length) {
                var carta = slot.cartasPato[j];
                if (carta.contadorTurnos !== undefined && carta.contadorTurnos !== null) {
                    if (self.contadorTurnos >= carta.contadorTurnos) {
                        cartasParaDevolver.push(carta);
                        slot.cartasPato = slot.cartasPato.splice(j, 1);
                    }
                }
                j ++;
            }
        }
        return cartasParaDevolver;
    };

    this.terminoUnTurno = function() {
        self.contadorTurnos++;
        return self.limpiarPoderesActivosPorUnTurno();
    };

    this.ponerMira = function(posicion) {
        self.cielos[posicion] = 1;
    };

    this.sacarMira = function(posicion) {
        self.cielos[posicion] = 0;
    };

    this.moverMira = function(posicion1, posicion2) {
        if (self.cielos[posicion1] === 1) {
            self.cielos[posicion1] = 0;
            self.cielos[posicion2] = 1;
        }
    };

    this.agregarCarta = function(carta) {
        assert.equal(carta === 'undefined', false);
        assert.equal(Array.isArray(carta), false, 'la carta pato no es valida');
        self.lago.push(new SlotCartaPato(carta));
    };

    this.moverTodosHaciaAdelante = function() {
        var sacado = self.lago.shift();
        return sacado;
    };

    this.disparar = function(posicion) {
        if (self.cielos[posicion] === 1) {
            var cartaPato = self.lago[posicion].cartasPato.pop();
            if (self.lago[posicion].cartasPato.length === 0) {
                self.lago.splice(posicion, 1);
            }
            self.sacarMira(posicion);
            return [cartaPato];
        }
        return [];
    };

    this.ocultarPato = function(juego, posicion, carta) {
        var slot = self.lago[posicion];
        carta.contadorTurnos = self.contadorTurnos + juego.jugadores.length;
        slot.agregarPato(carta);
    };

    this.intercambiar = function(posicion1, posicion2) {
        var aux = self.lago[posicion1];
        self.lago[posicion1] = self.lago[posicion2];
        self.lago[posicion2] = aux;
    };

    this.ocultarPatoDebajoDePato = function(posicion1, posicion2) {
        var slot1 = self.lago[posicion1];
        var slot2 = self.lago[posicion2];

        while (slot1.cartasPato.length > 0) {
            slot2.cartasPato.push(slot1.cartasPato.pop());
        }

        self.lago.splice(posicion1, 1);
    };

    this.reordenarObjetos = function(posiciones) {
        var nuevoLago = [];
        for (var i = 0; i < self.lago.length; i ++) {
            nuevoLago.push(self.lago[posiciones[i]]);
        }
        self.lago = nuevoLago;
    };

    this.serializar = function() {
        var slotsSerializados = [];
        self.lago.forEach(function(slotPato) {
           slotsSerializados.push(slotPato);
        });
      return {
          'cielos': self.cielos
          , 'lago': self.lago
      }
    };
};