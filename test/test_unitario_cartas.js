var expect = require('chai').expect;
var assert = require('assert');

var Juego = require('../juego/juego.js');
var AdministradorCartasEspeciales = require('../juego/administrador_cartas_especiales.js');
var AdministradorCartasPato = require('../juego/administrador_cartas_pato.js');
var CartaEspecialDarleAlDeAlLado = require('../juego/cartas_especiales/carta_especial_darle_al_de_al_lado.js');

var CartaEspecialApuntar = require('../juego/cartas_especiales/carta_especial_apuntar.js');
var CartaEspecialDisparar = require('../juego/cartas_especiales/carta_especial_disparar.js');
var CartaEspecialAvanzarPatos = require('../juego/cartas_especiales/carta_especial_avanzar_patos.js');
var CartaEspecialResucitar = require('../juego/cartas_especiales/carta_especial_resucitar.js');
var CartaPato = require('../juego/cartas_pato/carta_pato.js');
var Tablero = require('../juego/tablero.js');
var Jugador = require('../juego/jugador.js');

describe('test unitarios cartas', function () {

    it('carta disparar cuando no hay mira', function () {

        let contadorCartasDevueltas = 0;

        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        var juego = new Juego(instanciaAdministradorCartasPato, instanciaAdministradorCartasEspeciales);

        juego.devolverCartaEspecial = function() {

        };

        juego.devolverElementosSacadosDelTablero = function() {

        };

        juego.agregarUnaCartaPatoAlTablero = function() {
            contadorCartasDevueltas ++;
        };

        var tablero = new Tablero();
        var jugador1 = new Jugador('jota', 'azul');
        var jugador2 = new Jugador('rami', 'amarillo');

        tablero.cielo = [0, 0, 1, 0, 0, 0];
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));

        var cartaDisparar = new CartaEspecialDisparar();

        var jugadores = [jugador1, jugador2];
        var opciones = 1;

        cartaDisparar.ejecutar(juego, tablero, jugadores, opciones);

        assert.equal(contadorCartasDevueltas, 0, 'no se devolvio la carta pato matada')
    });

    it('carta disparar cuando hay mira', function () {
        var contadorCartasDevueltas = 0;

        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        var juego = new Juego(instanciaAdministradorCartasPato, instanciaAdministradorCartasEspeciales);

        juego.devolverCartaEspecial = function() {

        };

        juego.devolverElementosSacadosDelTablero = function() {

        };

        juego.agregarUnaCartaPatoAlTablero = function() {
            contadorCartasDevueltas ++;
        };

        var tablero = new Tablero();
        var jugador1 = new Jugador('jota', 'azul');
        var jugador2 = new Jugador('rami', 'amarillo');

        tablero.cielos = [1, 1, 1, 1, 1, 1];
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));

        var cartaDisparar = new CartaEspecialDisparar();

        var jugadores = [jugador1, jugador2];
        var opciones = 1;

        cartaDisparar.ejecutar(juego, tablero, jugadores, opciones);

        assert.equal(contadorCartasDevueltas, 1, 'no se devolvio la carta pato matada')
    });

    it('carta especial resucitar', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        var juego = new Juego(instanciaAdministradorCartasPato, instanciaAdministradorCartasEspeciales);

        var tablero = new Tablero();

        let jugador1 = juego.agregarJugador('jota', 'azul');
        let jugador2 = juego.agregarJugador('rami', 'amarillo');

        jugador1.patos.push(new CartaPato('azul'));

        tablero.cielos = [1, 1, 1, 1, 1, 1];
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));

        var cartaEspecialResucitar = new CartaEspecialResucitar();

        var jugadores = [jugador1, jugador2];
        var opciones = 'jota';
        var cartasPatoJugadorAntes = jugador1.patos.length;
        let cartasPatoEnJuegoAntes = instanciaAdministradorCartasPato.cartasPato.length;

        cartaEspecialResucitar.ejecutar(juego, tablero, jugadores, opciones);

        var cartasPatoJugadorDespues = jugador1.patos.length;
        let cartasPatoEnJuegoDespues = instanciaAdministradorCartasPato.cartasPato.length;
        assert.equal(cartasPatoJugadorAntes, cartasPatoJugadorDespues + 1, 'no se le saco la carta matada al jugador');
        assert.equal(cartasPatoEnJuegoAntes + 1, cartasPatoEnJuegoDespues, 'no se devolvio la carta pato matada al juego')
    });
});