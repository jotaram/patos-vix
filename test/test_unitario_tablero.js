var expect = require('chai').expect;
var assert = require('assert');

var Juego = require('../juego/juego.js');
var AdministradorCartasEspeciales = require('../juego/administrador_cartas_especiales.js');
var AdministradorCartasPato = require('../juego/administrador_cartas_pato.js');
var CartaEspecialDarleAlDeAlLado = require('../juego/cartas_especiales/carta_especial_darle_al_de_al_lado.js');

var CartaEspecialApuntar = require('../juego/cartas_especiales/carta_especial_apuntar.js');
var CartaEspecialDisparar = require('../juego/cartas_especiales/carta_especial_disparar.js');
var CartaEspecialAvanzarPatos = require('../juego/cartas_especiales/carta_especial_avanzar_patos.js');
var CartaPato = require('../juego/cartas_pato/carta_pato.js');
var Tablero = require('../juego/tablero.js');
var Jugador = require('../juego/jugador.js');
var CartaEspecialEsconderComoAvestruz = require('../juego/cartas_especiales/carta_especial_esconder_como_avestruz.js');

describe('test unitario tablero', function () {

    it('disparar en posicion cielo apuntado', function () {
        var tablero = new Tablero();

        tablero.cielos = [1, 1, 1, 1, 1, 1];
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('azul'));

        var opciones = 1;
        var cartasRecuperadas = tablero.disparar(parseInt(opciones));

        assert.equal(cartasRecuperadas.length, 1, 'no se devolvio la carta pato matada')
    });

    it('esconder como avestruz', function () {
        var tablero = new Tablero();

        var juego = new Juego();
        juego.agregarJugador('jota', 'azul');
        juego.agregarJugador('rami', 'amarillo');

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));

        var opciones = 1;
        var cartaEscondeComoAvestruz = new CartaEspecialEsconderComoAvestruz();
        var slot = tablero.lago[1];
        var cantidadCartasEnSlot = slot.cartasPato.length;

        tablero.ocultarPato(juego, parseInt(opciones), cartaEscondeComoAvestruz);

        var slotPost = tablero.lago[1];

        var nuevaCantidadCartasPatoEnSlot = slotPost.cartasPato.length;

        assert.equal(cantidadCartasEnSlot + 1, nuevaCantidadCartasPatoEnSlot, 'no se apilaron bien las cartas')
    });

    it('intercambiar con pato de adelante', function () {
        var tablero = new Tablero();

        var juego = new Juego();
        juego.agregarJugador('jota', 'azul');
        juego.agregarJugador('rami', 'amarillo');

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));
        tablero.agregarCarta(new CartaPato('naranja'));

        var pos1 = 1;
        var pos2 = 2;

        var slot1Pre = tablero.lago[pos1].cartasPato[0];
        var slot2Pre = tablero.lago[pos2].cartasPato[0];

        tablero.intercambiar(1, 2);

        var slot1Post = tablero.lago[pos1].cartasPato[0];
        var slot2Post = tablero.lago[pos2].cartasPato[0];

        assert.equal(slot1Pre.color, slot2Post.color, 'no se intercambiaron bien los patos`');
        assert.equal(slot2Pre.color, slot1Post.color, 'no se intercambiaron bien los patos');
    });

    it('esconder como avestruz esconde un pato', function () {
        var tablero = new Tablero();

        var juego = new Juego();
        juego.agregarJugador('jota', 'azul');
        juego.agregarJugador('rami', 'amarillo');

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));
        tablero.agregarCarta(new CartaPato('naranja'));

        var cartaEsconder = new CartaEspecialEsconderComoAvestruz();
        var pos1 = 1;

        tablero.ocultarPato(juego, pos1, cartaEsconder);

        var slotPos1 = tablero.lago[pos1];

        assert.equal(slotPos1.cartasPato.length, 2, 'en el slot no estaban bien las cartas pato y escudo');

        var carta1 = slotPos1.cartasPato[0];
        var carta2 = slotPos1.cartasPato[1];

        assert.equal(carta1.color, 'amarillo', 'no se intercambiaron bien los patos');
        assert.equal(carta2.codigo, cartaEsconder.codigo, 'no se intercambiaron bien los patos');
    });

    it('esconder como avestruz esconde un pato dura hasta siguiente turno jugador', function () {
        var tablero = new Tablero();

        var juego = new Juego();
        juego.agregarJugador('jota', 'azul');
        juego.agregarJugador('rami', 'amarillo');

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));
        tablero.agregarCarta(new CartaPato('naranja'));

        var cartaEsconder = new CartaEspecialEsconderComoAvestruz();
        var pos1 = 1;

        tablero.ocultarPato(juego, pos1, cartaEsconder);

        var slotPos1 = tablero.lago[pos1];

        assert.equal(slotPos1.cartasPato.length, 2, 'en el slot no estaban bien las cartas pato y escudo');

        var carta1 = slotPos1.cartasPato[0];
        var carta2 = slotPos1.cartasPato[1];

        assert.equal(carta1.color, 'amarillo', 'no se intercambiaron bien los patos');
        assert.equal(carta2.codigo, cartaEsconder.codigo, 'no se intercambiaron bien los patos');

        tablero.terminoUnTurno();

        slotPos1 = tablero.lago[pos1];

        assert.equal(slotPos1.cartasPato.length, 2, 'la carta avestruz no duro lo suficiente en juego');
    });

    it('esconder como avestruz esconde un pato no dura mas de un turno del jugador', function () {
        var tablero = new Tablero();

        var juego = new Juego();
        juego.agregarJugador('jota', 'azul');
        juego.agregarJugador('rami', 'amarillo');

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));
        tablero.agregarCarta(new CartaPato('naranja'));

        var cartaEsconder = new CartaEspecialEsconderComoAvestruz();
        var pos1 = 1;

        tablero.ocultarPato(juego, pos1, cartaEsconder);

        var slotPos1 = tablero.lago[pos1];

        assert.equal(slotPos1.cartasPato.length, 2, 'en el slot no estaban bien las cartas pato y escudo');

        var carta1 = slotPos1.cartasPato[0];
        var carta2 = slotPos1.cartasPato[1];

        assert.equal(carta1.color, 'amarillo', 'no se protegio la carta esperada');
        assert.equal(carta2.codigo, cartaEsconder.codigo, 'no se apilo bien la carta especial');

        var cartas = tablero.terminoUnTurno();

        slotPos1 = tablero.lago[pos1];

        assert.equal(cartas.length, 0, 'se devolvio una carta especial qeu no se tenia que devolver');
        assert.equal(slotPos1.cartasPato.length, 2, 'el slot no tiene dos cartas');

        cartas = tablero.terminoUnTurno();

        slotPos1 = tablero.lago[pos1];

        assert.equal(slotPos1.cartasPato.length, 1, 'la carta proteccion vive mas de lo debido en juego');
        assert.equal(cartas.length, 1, 'no se devolvio la carta especial');

    });

    it('avanzar patos', function () {
        var tablero = new Tablero();

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('amarillo'));

        var patoSacado = tablero.moverTodosHaciaAdelante();

        assert.equal(patoSacado.cartasPato[0].color, 'azul', 'no se saco bien un pato');
        assert.equal(patoSacado.cartasPato.length, 1, 'la carta pato se encolo mal');
    });

    it('ocultar pato debajo de pato', function () {
        var tablero = new Tablero();

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('naranja'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));

        var pos1 = 1;
        var pos2 = 2;

        tablero.ocultarPatoDebajoDePato(pos1, pos2);

        var nuevaPos2 = pos2 - 1;
        var slot2 = tablero.lago[nuevaPos2];

        assert.equal(tablero.lago.length, 5, 'no se saco el pato oculto, del tablero');
        assert.equal(slot2.cartasPato.length, 2, 'no se apilaron los patos');
        assert.equal(slot2.cartasPato[0].color, 'naranja', 'se puso el pato cobarde por arriba');
        assert.equal(slot2.cartasPato[1].color, 'amarillo', 'se perdio el pato que se queria esconder');
    });

    it('reordenar patos', function () {
        var tablero = new Tablero();

        tablero.agregarCarta(new CartaPato('azul'));
        tablero.agregarCarta(new CartaPato('amarillo'));
        tablero.agregarCarta(new CartaPato('naranja'));
        tablero.agregarCarta(new CartaPato('verde'));
        tablero.agregarCarta(new CartaPato('rosa'));
        tablero.agregarCarta(new CartaPato('violeta'));

        tablero.reordenarObjetos([5, 4, 3, 2, 1, 0]);

        assert.equal(tablero.lago[0].cartasPato[0].color, 'violeta', 'no se ordeno bien un pato');
        assert.equal(tablero.lago[1].cartasPato[0].color, 'rosa', 'no se ordeno bien un pato');
        assert.equal(tablero.lago[2].cartasPato[0].color, 'verde', 'no se ordeno bien un pato');
        assert.equal(tablero.lago[3].cartasPato[0].color, 'naranja', 'no se ordeno bien un pato');
        assert.equal(tablero.lago[4].cartasPato[0].color, 'amarillo', 'no se ordeno bien un pato');
        assert.equal(tablero.lago[5].cartasPato[0].color, 'azul', 'no se ordeno bien un pato');
    });
});