var expect = require('chai').expect;
var assert = require('assert');

var Juego = require('../juego/juego.js');
var AdministradorCartasEspeciales = require('../juego/administrador_cartas_especiales.js');
var AdministradorCartasPato = require('../juego/administrador_cartas_pato.js');
var CartaEspecialDarleAlDeAlLado = require('../juego/cartas_especiales/carta_especial_darle_al_de_al_lado.js');

var CartaEspecialApuntar = require('../juego/cartas_especiales/carta_especial_apuntar.js');
var CartaEspecialAvanzarPatos = require('../juego/cartas_especiales/carta_especial_avanzar_patos.js');
var CartaPato = require('../juego/cartas_pato/carta_pato.js');

describe('test integracion juego patos', function () {

    it('should start if there is at least two players', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        let patos = new Juego(instanciaAdministradorCartasPato
            , instanciaAdministradorCartasEspeciales);

        patos.agregarJugador('jota', 'azul');
        patos.agregarJugador('rami', 'amarillo');

        patos.iniciar('jota');

        assert.equal(patos.proximoJugador(), 'jota');
    });

    it('should start if every duck is on use', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        let patos = new Juego(instanciaAdministradorCartasPato
            , instanciaAdministradorCartasEspeciales);

        patos.agregarJugador('rami1', 'azul');
        patos.agregarJugador('rami2', 'amarillo');
        patos.agregarJugador('rami3', 'verde');
        patos.agregarJugador('rami4', 'violeta');
        patos.agregarJugador('rami5', 'naranja');
        patos.agregarJugador('rami6', 'rosa');

        patos.iniciar('rami1');

        assert.equal(patos.proximoJugador(), 'rami1');
        assert.equal(patos.jugadores.length, 6);

        var tablero = patos.tablero;

        assert.equal(tablero.cielos.length, tablero.tamanio);

        for (var j = 0; j < tablero.tamanio; j ++)
            assert.equal(tablero.cielos[j], 0);

        assert.equal(tablero.lago.length, 6);

        for (var i = 0; i < patos.tablero.tamanio; i ++) {
            assert.equal(tablero.lago[i].cartasPato.length, 1);
            assert.equal(tablero.lago[i].cartasPato[0] === undefined, false);
        }
    });

    it('el primer jugador debe poder jugar la carta apuntar como primer carta', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        instanciaAdministradorCartasEspeciales.cartasEspeciales = [
            new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
        ];

        let patos = new Juego(instanciaAdministradorCartasPato
            , instanciaAdministradorCartasEspeciales);

        patos.agregarJugador('jota', 'azul');
        patos.agregarJugador('rami', 'amarillo');

        patos.iniciar('jota');

        assert.equal(patos.proximoJugador(), 'jota', 'jugador que sigue');

        patos.jugarTurno('jota', 1, 1);

        var cu = instanciaAdministradorCartasEspeciales.cartasEspecialesUsadas;

        assert.equal(cu.length, 1, 'cantidad de cartas usadas');

        var cartaUsada = cu.pop();
        assert.equal(cartaUsada.codigo, (new CartaEspecialApuntar()).codigo, 'codigo de carta usada');

        assert.equal(patos.tablero.cielos[0], 1, 'la posicion 0 tiene mal la mira');
        assert.equal(patos.tablero.cielos[1], 0, 'la posicion 1 tiene mal la mira');
        assert.equal(patos.tablero.cielos[2], 0, 'la posicion 2 tiene mal la mira');
        assert.equal(patos.tablero.cielos[3], 0, 'la posicion 3 tiene mal la mira');
        assert.equal(patos.tablero.cielos[4], 0, 'la posicion 4 tiene mal la mira');
        assert.equal(patos.tablero.cielos[5], 0, 'la posicion 5 tiene mal la mira');
    });

    it('el primer jugador debe poder jugar la carta avanzar como primer carta', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

        instanciaAdministradorCartasEspeciales.cartasEspeciales = [
            new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
            , new CartaEspecialAvanzarPatos()
        ];

        let patos = new Juego(instanciaAdministradorCartasPato
            , instanciaAdministradorCartasEspeciales);

        patos.agregarJugador('jota', 'azul');
        patos.agregarJugador('rami', 'amarillo');

        patos.iniciar('jota');

        assert.equal(patos.proximoJugador(), 'jota', 'jugador que sigue');

        patos.jugarTurno('jota', 1, 1);

        var cu = instanciaAdministradorCartasEspeciales.cartasEspecialesUsadas;
        assert.equal(cu.length, 1, 'cantidad de cartas usadas');

        var cartaUsada = cu.pop();
        assert.equal(cartaUsada.codigo, (new CartaEspecialAvanzarPatos()).codigo, 'codigo de carta usada');

        assert.equal(patos.tablero.cielos[0], 0, 'la posicion 0 tiene mal la mira');
        assert.equal(patos.tablero.cielos[1], 0, 'la posicion 1 tiene mal la mira');
        assert.equal(patos.tablero.cielos[2], 0, 'la posicion 2 tiene mal la mira');
        assert.equal(patos.tablero.cielos[3], 0, 'la posicion 3 tiene mal la mira');
        assert.equal(patos.tablero.cielos[4], 0, 'la posicion 4 tiene mal la mira');
        assert.equal(patos.tablero.cielos[5], 0, 'la posicion 5 tiene mal la mira');

        assert.equal(patos.tablero.lago.length, 6);

    });

    it('el primer jugador debe poder jugar la darle al de al lado', function () {
        var instanciaAdministradorCartasPato = new AdministradorCartasPato();
        var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();


        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));

        instanciaAdministradorCartasPato.cartasAguaBase = [];
        instanciaAdministradorCartasEspeciales.cartasEspeciales = [
            new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()

            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()
            , new CartaEspecialApuntar()

            , new CartaEspecialApuntar()
            , new CartaEspecialDarleAlDeAlLado()
            , new CartaEspecialApuntar()
        ];

        let patos = new Juego(instanciaAdministradorCartasPato
            , instanciaAdministradorCartasEspeciales);

        patos.agregarJugador('jota', 'azul');
        patos.agregarJugador('rami', 'amarillo');

        patos.iniciar('jota');

        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('azul'));
        instanciaAdministradorCartasPato.cartasPato.push(new CartaPato('amarillo'));


        assert.equal(patos.proximoJugador(), 'jota', 'jugador que sigue');

        patos.jugarTurno('jota', 0, 1);
        var cu = instanciaAdministradorCartasEspeciales.cartasEspecialesUsadas;
        assert.equal(cu.length, 1, 'cantidad de cartas usadas');
        var cartaUsada = cu[0];
        assert.equal(cartaUsada.codigo, (new CartaEspecialApuntar()).codigo, 'codigo de carta usada');
        assert.equal(patos.tablero.cielos[0], 1, 'la posicion 0 tiene mal la mira');
        assert.equal(patos.tablero.cielos[1], 0, 'la posicion 1 tiene mal la mira');
        assert.equal(patos.tablero.cielos[2], 0, 'la posicion 2 tiene mal la mira');
        assert.equal(patos.tablero.cielos[3], 0, 'la posicion 3 tiene mal la mira');
        assert.equal(patos.tablero.cielos[4], 0, 'la posicion 4 tiene mal la mira');
        assert.equal(patos.tablero.cielos[5], 0, 'la posicion 5 tiene mal la mira');
        assert.equal(patos.tablero.lago.length, 6);
        assert.equal(patos.proximoJugador(), 'rami', 'jugador que sigue');

        patos.jugarTurno('rami', 0, 2);

        cu = instanciaAdministradorCartasEspeciales.cartasEspecialesUsadas;
        assert.equal(cu.length, 2, 'cantidad de cartas usadas');

        cartaUsada = cu[1];
        assert.equal(cartaUsada.codigo, (new CartaEspecialApuntar()).codigo, 'codigo de carta usada por adversario');
        assert.equal(patos.tablero.cielos[0], 1, 'la posicion 0 tiene mal la mira');
        assert.equal(patos.tablero.cielos[1], 1, 'la posicion 1 tiene mal la mira');
        assert.equal(patos.tablero.cielos[2], 0, 'la posicion 2 tiene mal la mira');
        assert.equal(patos.tablero.cielos[3], 0, 'la posicion 3 tiene mal la mira');
        assert.equal(patos.tablero.cielos[4], 0, 'la posicion 4 tiene mal la mira');
        assert.equal(patos.tablero.cielos[5], 0, 'la posicion 5 tiene mal la mira');
        assert.equal(patos.tablero.lago.length, 6);
        assert.equal(patos.proximoJugador(), 'jota', 'jugador que sigue');

        patos.jugarTurno('jota', 0, 1);

        cu = instanciaAdministradorCartasEspeciales.cartasEspecialesUsadas;
        assert.equal(cu.length, 3, 'cantidad de cartas usadas');
        cartaUsada = cu[2];
        assert.equal(cartaUsada.codigo, (new CartaEspecialDarleAlDeAlLado()).codigo, 'codigo de carta usada');
        assert.equal(patos.tablero.cielos[0], 1, 'la posicion 0 tiene mal la mira');
        assert.equal(patos.tablero.cielos[1], 0, 'la posicion 1 tiene mal la mira');
        assert.equal(patos.tablero.cielos[2], 0, 'la posicion 2 tiene mal la mira');
        assert.equal(patos.tablero.cielos[3], 0, 'la posicion 3 tiene mal la mira');
        assert.equal(patos.tablero.cielos[4], 0, 'la posicion 4 tiene mal la mira');
        assert.equal(patos.tablero.cielos[5], 0, 'la posicion 5 tiene mal la mira');
        assert.equal(patos.tablero.lago.length, 6);
    });

});