var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.get('/favicon.ico', function(req, res){
    res.sendFile(__dirname + '/favicon.ico');
});

app.use(express.static('public'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

var Juego = require('./juego/juego.js');
var AdministradorCartasEspeciales = require('./juego/administrador_cartas_especiales.js');
var AdministradorCartasPato = require('./juego/administrador_cartas_pato.js');

var instanciaAdministradorCartasPato = new AdministradorCartasPato();
var instanciaAdministradorCartasEspeciales = new AdministradorCartasEspeciales();

let patos = new Juego(instanciaAdministradorCartasPato
    , instanciaAdministradorCartasEspeciales);

io.on('connection', function(socket){
    console.log('a user connected');

    // le notifico de todos los colores desabilitados


    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('chat-message', function(msg){
        console.log('message: ' + msg);

        socket.broadcast.emit('chat-message', msg);
    });

    socket.on('confirmar-jugador', function(nombreJugador, color){
        console.log('se confirmo el jugador: ' + nombreJugador + ' con color: ' + color);

        patos.agregarJugador(nombreJugador, color);

        socket.emit('mensaje-jugador-confirmado', 'el jugador fue aceptado');

        io.emit('desabilitar-color', nombreJugador, color);
    });

    socket.on('comenzar-juego', function(nombreJugador){
        console.log('comenzando juego');
        patos.iniciar(nombreJugador);


        console.log(patos.serializar());

        io.emit('mensaje-juego-comenzado', patos.jugadores);
        io.emit('notificacion-juego', 'le toca jugar a: ' + patos.proximoJugador());
        io.emit('notificacion-estado-juego', patos.serializar());
    });

    socket.on('jugar-carta', function(jugador, carta, opciones) {
        console.log('el jugador ' + jugador + ' jugo con la carta ' + carta);
        var explicacion = patos.jugarTurno(jugador, carta, opciones);

        io.emit('notificacion-juego', jugador + ': ' + explicacion);
        io.emit('notificacion-estado-juego', patos.serializar());

        if (patos.hayGanador()) {
            io.emit('notificacion-juego', 'el juego termino, el ganador es: ' + patos.jugadorGanador());
        }
        else {
            io.emit('notificacion-juego', 'le toca jugar a: ' + patos.proximoJugador());
        }

        console.log(patos.serializar())
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
