FROM node:8.15-alpine
ADD . /app
WORKDIR /app
RUN npm install
ENTRYPOINT node index.js